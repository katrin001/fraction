package com.example.student1.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private EditText numerator1;
    private EditText numerator2;
    private EditText denumerator1;
    private EditText denumerator2;
    private TextView rezn;
    private TextView rezd;
    Button multiplication;
    Button division;
    Button addition;
    Button subtraction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numerator1 = (EditText) findViewById(R.id.numerator1);
        numerator2 = (EditText) findViewById(R.id.numerator2);
        denumerator1 = (EditText) findViewById(R.id.denumerator1);
        denumerator2 = (EditText) findViewById(R.id.denumerator2);
        multiplication = (Button) findViewById(R.id.multiplication);
        division = (Button) findViewById(R.id.division);
        addition = (Button) findViewById(R.id.addition);
        subtraction = (Button) findViewById(R.id.subtraction);
        rezn = (TextView) findViewById(R.id.RezNum);
        rezd = (TextView) findViewById(R.id.RezDenum) ;



        View.OnClickListener add = new View.OnClickListener() {
            @Override
            public void onClick(View v){

                int num1, num2, denum1;
                int denum2, nrez, drez;

                String n1 = numerator1.getText().toString();
                String n2 = numerator2.getText().toString();
                String d1 = denumerator1.getText().toString();
                String d2 = denumerator2.getText().toString();


                num1 = Integer.parseInt(n1);
                num2 = Integer.parseInt(n2);
                denum1 = Integer.parseInt(d1);
                denum2 = Integer.parseInt(d2);

                Fraction f1 = new Fraction(num1, denum1);
                Fraction f2 = new Fraction(num2, denum2);
                Fraction f3 = f1.add(f2);

                rezn.setText(String.valueOf(f3.getNominator()));
                rezd.setText(String.valueOf(f3.getDenominator()));
            }
        };

        addition.setOnClickListener(add);

        View.OnClickListener sub = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1, num2, denum1;
                int denum2, nrez, drez;

                String n1 = numerator1.getText().toString();
                String n2 = numerator2.getText().toString();
                String d1 = denumerator1.getText().toString();
                String d2 = denumerator2.getText().toString();


                num1 = Integer.parseInt(n1);
                num2 = Integer.parseInt(n2);
                denum1 = Integer.parseInt(d1);
                denum2 = Integer.parseInt(d2);

                Fraction f1 = new Fraction(num1, denum1);
                Fraction f2 = new Fraction(num2, denum2);
                Fraction f3 = f1.sub(f2);

                rezn.setText(String.valueOf(f3.getNominator()));
                rezd.setText(String.valueOf(f3.getDenominator()));
            }
        };

        subtraction.setOnClickListener(sub);





        View.OnClickListener div = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int num1, num2, denum1;
                int denum2, nrez, drez;

                String n1 = numerator1.getText().toString();
                String n2 = numerator2.getText().toString();
                String d1 = denumerator1.getText().toString();
                String d2 = denumerator2.getText().toString();


                num1 = Integer.parseInt(n1);
                num2 = Integer.parseInt(n2);
                denum1 = Integer.parseInt(d1);
                denum2 = Integer.parseInt(d2);

                Fraction f1 = new Fraction(num1, denum1);
                Fraction f2 = new Fraction(num2, denum2);
                Fraction f3 = f1.div(f2);

                rezn.setText(String.valueOf(f3.getNominator()));
                rezd.setText(String.valueOf(f3.getDenominator()));
            }
        };

        division.setOnClickListener(div);


        View.OnClickListener mult = new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int num1, num2, denum1;
                int denum2, nrez, drez;

                String n1 = numerator1.getText().toString();
                String n2 = numerator2.getText().toString();
                String d1 = denumerator1.getText().toString();
                String d2 = denumerator2.getText().toString();


                num1 = Integer.parseInt(n1);
                num2 = Integer.parseInt(n2);
                denum1 =
                        Integer.parseInt(d1);
                denum2 = Integer.parseInt(d2);

                Fraction f1 = new Fraction(num1, denum1);
                Fraction f2 = new Fraction(num2, denum2);
                Fraction f3 = f1.mult(f2);


                rezn.setText(String.valueOf(f3.getNominator()));
                rezd.setText(String.valueOf(f3.getDenominator()));

            }


        };

        multiplication.setOnClickListener(mult);
    }


}